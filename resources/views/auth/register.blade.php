@extends('layouts.auth-1')

@section('content')
<div class="feature-panel">
                      <img  class="logo" src="{{asset('img/logo.PNG')}}">
           <a href="{{url('login')}}" class="trans-bt"> LOGIN</a>

        </div>
        <div class="main-panel">
            <div class="main-panel-table">
                <div class="main-panel-tabel-cell">
                    <div class="main_login_content">
                        <div class="login_text">
                            Sign Up
                        </div>
                        <div class="login_text_small">
                            Enter your details below.
                        </div>
                        <div class="login_form">
                             <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} auth_login_forms" style="margin-left:0px; margin-right:0px">
                                    <label for="name" class="auth_label">Name</label>

                                    <div class="text_container">
                                        <input id="name" type="text" class="form-control auth_input" placeholder="Fullname" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} auth_login_forms" style="margin-left:0px; margin-right:0px">
                                    <label for="email" class="auth_label">E-Mail Address</label>

                                    <div class="text_container">
                                        <input id="email" type="email" class="form-control auth_input" placeholder="Your email" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} auth_login_forms" style="margin-left:0px; margin-right:0px">
                            <label for="password" class="auth_label">Password</label>

                            <div class="text_container">
                                <input id="password" type="password" placeholder="Password" class="form-control auth_input" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                         </div>

                         <div class=" form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                            <label for="password-confirm" class="auth_label">Confirm Password</label>

                            <div class="text_container">
                                <input id="password-confirm" type="password" placeholder="Comfirm your password" class="form-control auth_input" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                           <label for="password" class="auth_label">Sign up As</label>

                           <div class="text_container">
                               <select name="user_type_id" required class="form-control auth_input chosen-select-no-single">
                                  <option value="1">Professor</option>
                                  <option value="2">Student</option>
                                  <option value="3">Assistant</option>
                               </select>      
                           </div>
                        </div>


                        <div class="form-group" style="margin-left:0px; margin-right:0px">
                            <input type="submit" value="Sign Up" class="login_btn">
                        </div>


                             </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
