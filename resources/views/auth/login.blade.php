@extends('layouts.auth-1')

@section('content')
        <div class="feature-panel">
           <img  class="logo" src="{{asset('img/logo.PNG')}}">
           <a href="{{url('register')}}" class="trans-bt"> GET STARTED</a>
        </div>
        <div class="main-panel">
            <div class="main-panel-table">
                <div class="main-panel-tabel-cell">
                    <div class="main_login_content">
                        <div class="login_text">
                            Sign In
                        </div>
                        <div class="login_text_small">
                            Enter your details below.
                        </div>
                        <div class="login_form">
                             <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} auth_login_forms" style="margin-left:0px; margin-right:0px">
                                    <label for="email" class="auth_label">E-Mail Address</label>

                                    <div class="text_container">
                                        <input id="email" type="email" class="form-control auth_input" placeholder="Your email" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} auth_login_forms" style="margin-left:0px; margin-right:0px">
                            <label for="password" class="auth_label">Password</label>

                            <div class="text_container">
                                <input id="password" type="password" placeholder="Password" class="form-control auth_input" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                         </div>

                         <div class="form-group" style="margin-left:0px; margin-right:0px">
                            <div class="pull-left">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>

                            <a class="btn btn-link auth_forgot_pass" href="{{ url('/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                        </div>

                        <div class="form-group" style="margin-left:0px; margin-right:0px">
                            <input type="submit" value="Login" class="login_btn">
                        </div>


                             </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
