<li class="side-bar-nav-item nav-active" id="home">
    <a href="{{url('assistants/home')}}"><img src="{{asset('img/overview.PNG')}}"> <span>Overview</span></a>
</li> 

<li class="side-bar-nav-item" id="assignments">
    <a href="{{url('assistants/assignments')}}"><img src="{{asset('img/assignment.PNG')}}"> <span>Assignments</span></a>
</li>

<li class="side-bar-nav-item" id="leaderboard">
    <a href="{{url('assistants/leaderboard')}}"><img style="width: 15px;" src="{{asset('img/leaderboard.PNG')}}"> <span>Leaderboard</span></a>
</li>