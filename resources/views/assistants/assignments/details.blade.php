@extends('layouts.app')

@section('content')


<div class="content">
        <div class="content-header">
            <span class="page-title">{{$assignment->title}}</span>

             @if(Session::has('success'))
                <div class="alert alert-success">
                      <p>{{Session::get('success','')}}</p>
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">
                      <p>{{Session::get('error','')}}</p>
                </div>
            @endif


            <div class="page-controls">
{{--               <a href="{{url('professors/assignments/create')}}" class="btn btn-primary pull-right"> </a>
 --}}            </div>


        </div>
        <div class="content-divider"></div>

        <div class="content-body">
          
          <p style="font-weight: 600">INSTRUCTIONS</p>
          <P style="margin-bottom: 40px" >{{$assignment->description}}</P>


          <table class="table table-striped">
              <thead>
                  <th>STUDENT NAME</th>
                  <th>SUBMITTED DATE</th>
                  <th>MARKER</th>
                  <th>SCORE</th>
                  <th>ACTION</th>

              </thead>

              <tbody>
                 
                  @foreach($assignment->responses()->get() as $response)
                        <tr>
                            <td>{{$response->student()->value('name')}}</td>
                            <td>{{$response->created_at}}</td>
                            <td>{{$response->marker()->value('name') ?: "NA"}}</td>
                            <td>{{$response->student_score}}</td>
                            <td>
                              @if($assignment->expiry_date < Carbon\Carbon::today())
                                  @if($response->status_id == 1)
                                    <a href="{{url('/assignments/submitted/'.$response->file)}}">
                                        <img style="width: 20px" src="{{asset('img/download.png')}}">
                                    </a>

                                    <a href="#" data-toggle="modal" data-target="#score-modal" data-id="{{$response->id}}">
                                      <img style="width: 20px" src="{{asset('img/upload.png')}}">
                                    </a>
                                  @else
                                      @if($response->status_id == 2 && $response->marker_id == Auth::user()->id)
                                      <a href="#" data-toggle="modal" data-target="#score-modal" data-id="{{$response->id}}">
                                            <img style="width: 20px" src="{{asset('img/edit.png')}}">
                                        </a>
                                      @else

                                        <p>-</p>

                                       @endif

                                  @endif
                              @else
                                Not Due
                              @endif
                            </td>

                        </tr>
                    @endforeach
              </tbody>
          </table>

        </div>
    </div>
@endsection

<div class="modal fade" id="score-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
           <div class="modal-dialog">
               <div class="modal-content">

                   <!--Modal header-->
                   <div class="modal-header">
                       <button data-dismiss="modal" class="close" type="button">
                       <span aria-hidden="true">&times;</span>
                       </button>
                       <h4 class="modal-title page_title_inner">Submit Score</h4>
                   </div>

                   <!--Modal body-->
                   <div class="modal-body">
                     <div class="row">
                       <form method="POST" action="{{url('assistants/assignments/submit-score')}}">
                       {{ csrf_field() }}
                       <div class="col-sm-12">
                           <input type="hidden" name="response_id" id="response_id" value="">
                           
                           <div class="form-group auth_login_forms" style="">
                                <label for="code" class="control-label client_label auth_label">Score</label>
                                <div class="text_container">
                                    <input type="number" name="score" class="form-control auth_input" required>  
                                </div>
                           </div>
                           
                           <div class="form-group auth_login_forms" style="">
                                <label for="remarks" class="control-label client_label auth_label">Remarks</label>
                                <div class="text_container">
                                    <textarea name="remarks" class="form-control auth_input" rows="4" required></textarea>  
                                </div>
                           </div>
                        
                       </div>
                     </div>
                   </div>

                   <!--Modal footer-->
                   <div class="modal-footer">
                       <button class="save_btn" type="submit">Save</button>
                   </div>
                   </form>
               </div>
           </div>
       </div>

@section('scripts')
 <script type="text/javascript">
 $('#assignments').addClass('active-link')

 $('#score-modal').on('show.bs.modal', function (e) {
        var assignmentBnt = $(e.relatedTarget); 
        var id =  assignmentBnt.attr('data-id');
        console.log(id);
        $('#response_id').val(id);
     });

 </script>
 @endsection
