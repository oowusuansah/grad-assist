@extends('layouts.app')

@section('content')


<div class="content">
        <div class="content-header">
            <span class="page-title">ASSIGMENTS</span>

             @if(Session::has('success'))
                <div class="alert alert-success">
                      <p>{{Session::get('success','')}}</p>
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">
                      <p>{{Session::get('error','')}}</p>
                </div>
            @endif


            <div class="page-controls">
{{--               <a href="{{url('professors/assignments/create')}}" class="btn btn-primary pull-right"> create Assignment</a>
 --}}            </div>


        </div>
        <div class="content-divider"></div>

        <div class="content-body">
          
          <table class="table table-striped">
              <thead>
                  <th>#</th>
                  <th>ASSIGMENT</th>
                  <th>COURSE</th>
                  <th>DUE DATE</th>
                  <th>SUBMITTED</th>
                  <th>MARKED</th>
                  <th>HIGHEST SCORE</th>

              </thead>

              <tbody>
                 
                  @foreach($assignments as $assignment)
                        <tr>
                            <td></td>
                            <td><a href="{{url('assistants/assignments/'.$assignment->id.'/submitted')}}">{{$assignment->title}}</a></td>
                            <td>{{$assignment->course()->value('name')}}</td>
                            <td>{{$assignment->expiry_date}}</td>
                            <td>{{$assignment->responses()->count()}}</td>
                           <td>{{$assignment->responses()->where('status_id','>',1)->count()}}</td>
                            <td>{{$assignment->responses()->where('status_id','>',1)->max('student_score')}}</td>

                        </tr>
                    @endforeach
              </tbody>
          </table>

        </div>
    </div>
@endsection

{{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum. --}}
@section('scripts')
 <script type="text/javascript">
 $('#assignments').addClass('active-link')
 </script>
 @endsection
