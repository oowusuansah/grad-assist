@extends('layouts.app')

@section('content')


<div class="content">
        <div class="content-header">
            <span class="page-title">LEADERBOARD</span>

             @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>Assignment saved Successsfully</p>
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">
                    <p>Assignment failed to save. Try Again</p>
                </div>
            @endif


            <div class="page-controls">
{{--               <a href="{{url('professors/assignments/create')}}" class="btn btn-primary pull-right"> create Assignment</a>
 --}}            </div>


        </div>
        <div class="content-divider"></div>

        <div class="content-body">
          
          <table class="table table-striped">
              <thead>
                  <th>RANK</th>
                  <th>ASSISTANT</th>
                  <th>MARKED ASSIGMENTS</th>
                  <th>TOTAL RATINGS</th>

              </thead>

              <tbody>
                 
                 @foreach($rankings as $rank)
                  <tr>
                    <td>{{$rank['rank']}}</td>
                    <td>
                    <div class="rank-icon" style="background-image:url({{ asset('media/users/'.$rank['image']) }}); background-size:cover;background-repeat: no-repeat;;background-position: 50% 50%"></div>
                      <p style="margin-top: 10px">{{$rank['name']}}</p>
                    </td>
                    <td>{{$rank['assignments']}}</td>
                    <td>{{$rank['total_score']}}</td>
                  </tr>
                 @endforeach
              </tbody>
          </table>

        </div>
    </div>
@endsection

{{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum. --}}
@section('scripts')
 <script type="text/javascript">
 $('#leaderboard').addClass('active-link')
 </script>
 @endsection
