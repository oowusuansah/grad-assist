<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
  <link rel="stylesheet" href="{{url('/')}}/bower_components/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/bower_components/chosen/chosen.min.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/bower_components/jquery-bar-rating/dist/themes/bars-square.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/bower_components/jquery-bar-rating/dist/themes/fontawesome-stars.css">
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body style="background: #fff">
    <div id="app">
        <nav class="navbar navbar-inverse navbar-fixed-top nav-style">
            <div class="container nav-container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img  class="logo" src="{{asset('img/logo.PNG')}}">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                       <li class="nav-right-item"><a href="#"  data-toggle="modal" data-target="#profile-modal"  title="Profile"><img class="nav-icon" src="{{asset('img/user.PNG')}}"></a></li>
                       <li class="nav-right-item"><a href="{{url('logout')}}" title="Logout"><img class="nav-icon" style="width: 25px" src="{{asset('img/power-2.PNG')}}"></a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="main-container row">
            <div class="col-md-2 side-bar">
                <div class="side-bar-top">
                    <div class="side-bar-avatar" style="background-image:url({{ asset('media/users/'.Auth::user()->image) }}); background-size:cover;background-repeat: no-repeat;background-position: 50% 50%"></div>
                    <div class="side-bar-top-details">
                        <span class="side-bar-name">{{Auth::user()->name}}</span>
                        <p class="side-bar-usertype">{{Auth::user()->userType()->value('name')}}</p>
                    </div>
                </div>
                <ul class="side-bar-nav">
                   @if(Auth::user()->user_type_id == 1)

                        {{-- Professor --}}
                       @include('professors.links')
    
                   @elseif(Auth::user()->user_type_id == 2)
 

                         {{-- Professor --}}
                        @include('students.links')

                   @else

                         {{-- Professor --}}
                        @include('assistants.links')


                   @endif
                </ul>
            </div>
            <div class="col-md-10 main-content">
                @yield('content')
            </div>
        </div>


    </div>

    <div class="modal fade" id="profile-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
               <div class="modal-dialog">
                   <div class="modal-content">

                       <!--Modal header-->
                       <div class="modal-header">
                           <button data-dismiss="modal" class="close" type="button">
                           <span aria-hidden="true">&times;</span>
                           </button>
                           <h4 class="modal-title page_title_inner">Profile</h4>
                       </div>

                       <!--Modal body-->
                       <div class="modal-body">
                         <div class="row">
                           <form method="POST" action="{{url('users/profile/update')}}" enctype="multipart/form-data">
                           {{ csrf_field() }}
                           <div class="col-sm-12">
                               
                               <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                                   <div class="image-preview" style="">

                                     @if(Auth::user()->image)
                                      <img src="{{ asset('media/users/'.Auth::user()->image) }}" style="height: 100px;width:100px;border-radius: 50px">   
                                      @endif            
                                   </div>
                                   <div class="inp_file">
                                       <label class="pro_img" for="in_file">select image</label>
                                       <input type="file" id="in_file" name="image"> 
                                   </div>
                               </div>


                               <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                                   <label for="name" class="auth_label">Name</label>

                                   <div class="text_container">
                                       <input id="name" value="{{Auth::user()->name}}" type="text" class="form-control auth_input" placeholder="Your Name" name="name" required>
                                   </div>
                               </div> 

                               @if(Auth::user()->user_type_id > 1)
                                 <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                                     <label for="alias" class="auth_label">Name Alias / Username</label>

                                     <div class="text_container">
                                         <input id="alias" value="{{Auth::user()->alias}}" type="text" class="form-control auth_input" placeholder="Your alias" name="alias" required>
                                     </div>
                                 </div>
                               @endif

                           </div>
                         </div>
                       </div>

                       <!--Modal footer-->
                       <div class="modal-footer">
                           <button class="save_btn pull-right" type="submit">Save</button>
                       </div>
                       </form>
                   </div>
               </div>
           </div>


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{url('/')}}/bower_components/chosen/chosen.jquery.min.js"></script>
    <script src="{{url('/')}}/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="{{url('/')}}/bower_components/moment/min/moment.min.js"></script>
    <script src="{{url('/')}}/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="{{url('/')}}/bower_components/chart.js/dist/Chart.js"></script>
    <script src="{{url('/')}}/bower_components/chart.js/dist/Chart.bundle.js"></script>
    <script src="{{url('/')}}/bower_components/chart.js/samples/utils.js"></script>
   
    <script type="text/javascript">
        setTimeout(function() {
            $('.alert').hide('fast');
        }, 3000);

        
        $(function () {
          "use strict";
          var configChosen = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
            '.chosen-select-width'     : {width:"100%"}
          }

          for (var selector in configChosen) {
            $(selector).chosen(configChosen[selector]);
          }
        });


        $('#date-range-picker').daterangepicker({
            autoUpdateInput: false
        });

        $('#date-range-picker').on('apply.daterangepicker', function(ev, picker) {
             $('#date-from').val(picker.startDate.format('YYYY-MM-DD'));
             $('#date-to').val(picker.endDate.format('YYYY-MM-DD'));
         });

         $('#date-range-picker').on('cancel.daterangepicker', function(ev, picker) {
             $('#date-from').val('');
             $('#date-to').val('');
         });


          $(document).on('change','#in_file', function(event) {
       
            console.log(this.files.length);
             for (var i = this.files.length - 1; i >= 0; i--) {

                 $('.image-preview').html('<img style="height: 100px;width:100px;border-radius: 50px" src="' + window.URL.createObjectURL(this.files[i]) + '">');
                 // console.log(window.URL.createObjectURL(this.files[i]));
             }

        }); 
    </script>


        @yield('scripts')

</body>
</html>
