@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="content-header">
            <span class="page-title">OVERVIEW</span>
        </div>
        <div class="content-divider"></div>
        <div class="content-body">
        	
        	<div class="row top-filter" hidden>
        		<div class="col-md-3">
        			<select class="form-control control-ex" id="duration">
        				<option value="1">Semester</option>
        				<option value="2">Week</option>
        				<option value="2">Month</option>
        			</select>
        		</div>
        		<div class="col-md-3">
        			<select class="form-control control-ex" id="course">
        				<option value="9">All Courses</option>
        				<option value="1">Course 1</option>
        				<option value="2">Course 2</option>
        				<option value="3">Course 3</option>
        				<option value="4">Course 4</option>
        			</select>
        		</div>
        		<div class="col-md-3">
        			<div class="form-control control-ex" id="date-range-box">
        				<img class="control-icon" src="{{asset('img/calendar.png')}}" id="date-range-picker">
        				<input type="text" name="date-range" class="date-range" id="date-from" readonly>
        				<img class="control-icon" src="{{asset('img/arrow_right.png')}}">
        				<input type="text" name="date-range" class="date-range" id="date-to" readonly>
        			</div>
        		</div>
        		<div class="col-md-1">
        			<button class="form-control control-ex control-btn" id="filter-btn">
        				<img class="control-icon-btn" src="{{asset('img/filter.png')}}"> Filter
        			</button>
        		</div>
        		
        	</div>

        	<div class="widget-content">
	     		<div class="row stats-box">
	     			<div class="col-md-3 stat-box">
	     				<span class="stat-count">{{$assignmentsCount}}</span>
	     				<span class="stat-name">ALL ASSIGMENTS</span>
	     			</div>
	     			<div class="col-md-3 stat-box">
	     				<span class="stat-count">{{$avgScore}}</span>
	     				<span class="stat-name">AVERAGE SCORE</span>
	     			</div>
	     			<div class="col-md-3 stat-box">
	     				<span class="stat-count">{{$maxScore}}</span>
	     				<span class="stat-name">HIGHEST SCORE</span>
	     			</div>
	     			<div class="col-md-3 stat-box no-border">
	     				<span class="stat-count">{{$minScore}}</span>
	     				<span class="stat-name">LOWEST SCORE</span>
	     			</div>
	     		</div>
	     		<div class="table-box" style="width: 70%; margin: auto;">
	     			 <canvas id="canvas"></canvas>
	     		</div>
	     	</div>

        </div>
    </div>
@endsection

@section('scripts')
 <script type="text/javascript">
 $('#home').addClass('active-link')

 var color = Chart.helpers.color;
 var barChartData = {
     labels: {!!json_encode($codes)!!},
     datasets: [{
         label: 'Average Course score',
         backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
         borderColor: window.chartColors.red,
         borderWidth: 1,
         data:{{json_encode($avgs)}}
     }]

 };

 window.onload = function() {
     var ctx = document.getElementById("canvas").getContext("2d");
     window.myBar = new Chart(ctx, {
         type: 'bar',
         data: barChartData,
         options: {
             responsive: true,
             legend: {
                 position: 'bottom',
             },
             title: {
                 display: true,
                 text: 'Average Course Score this Semester'
             },

             scales: {
            	yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
         }
     });

 };
 </script>
 @endsection
