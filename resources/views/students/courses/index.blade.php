@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="content-header">
            <span class="page-title">COURSES</span>
            <div class="page-controls">

              @if(Session::has('success'))
                  <div class="alert alert-success">
                      <p>{{Session::get('success','')}}</p>
                  </div>
              @elseif(Session::has('error'))
                  <div class="alert alert-danger">
                      <p>{{Session::get('error','')}}</p>
                  </div>
              @endif

              <button class="control_btn pull-right" data-toggle="modal" data-target="#create-course-modal"><i class="fa fa-plus-square"></i> &nbsp; Register Course</button>
            </div>
        </div>
        <div class="content-divider"></div>
        <div class="content-body">
          
          <table class="table table-striped">
              <thead>
                  <th>#</th>
                  <th>COURSE CODE</th>
                  <th>COURSE NAME</th>
                  <th>DESCRIPTION</th>
                  <th>NO. ASSIGNMENTS</th>
              </thead>

              <tbody>
                  @foreach($registeredCourses as $course)
                      <tr>
                          <td></td>
                          <td>{{$course->code}}</td>
                          <td>{{$course->name}}</td>
                          <td>{{$course->name}}</td>
                          <td>{{$course->assignments()->count()}}</td>
                      </tr>
                  @endforeach
              </tbody>
          </table>

        </div>
    </div>
@endsection
{{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
 --}}

<div class="modal fade" id="create-course-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
           <div class="modal-dialog">
               <div class="modal-content">

                   <!--Modal header-->
                   <div class="modal-header">
                       <button data-dismiss="modal" class="close" type="button">
                       <span aria-hidden="true">&times;</span>
                       </button>
                       <h4 class="modal-title page_title_inner">Create Course</h4>
                   </div>

                   <!--Modal body-->
                   <div class="modal-body">
                     <div class="row">
                       <form method="POST" action="{{url('students/courses/register')}}">
                       {{ csrf_field() }}
                       <div class="col-sm-12">
                           
                           <div class="form-group auth_login_forms" style="">
                                <label for="email" class="control-label client_label auth_label">Select Courses</label>
                                <div class="text_container">
                                    <select name="courses[]" multiple required class="form-control auth_input chosen-select chosen-select-width" style="width: 100%">
                                       @foreach($courses as $course)
                                        <option  value="{{$course->id}}">
                                            {{$course->code. '-' .$course->name}}
                                        </option>
                                       @endforeach
                                    </select>  
                                </div>  
                           </div>
                          
                       </div>
                     </div>
                   </div>

                   <!--Modal footer-->
                   <div class="modal-footer">
                       <button class="save_btn" type="submit">Register</button>
                   </div>
                   </form>
               </div>
           </div>
       </div>


@section('scripts')
 <script type="text/javascript">
 $('#courses').addClass('active-link')


 $('#create-course-modal').on('show.bs.modal', function (e) {
            $('.chosen-container').attr('style','width:100%');
        });

 </script>
 @endsection


  