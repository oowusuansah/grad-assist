@extends('layouts.app')

@section('content')


<div class="content">
        <div class="content-header">
            <span class="page-title">{{$assignment->title}}</span>

            <?php $response = $assignment->studentResponse(Auth::user()->id)->first();?>


             @if(Session::has('success'))
                <div class="alert alert-success">
                      <p>{{Session::get('success','')}}</p>
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">
                      <p>{{Session::get('error','')}}</p>
                </div>
            @endif


            <div class="page-controls">
              @if($assignment->active == 1 && $assignment->expiry_date >= Carbon\Carbon::today())
                   
                  @if($response && $response->status_id == 1)
    
                      <a href="{{url('/').'/assignments/'.$assignment->file}}" class="control_btn"> 
                        <i class="fa fa-cloud-download"></i> &nbsp;  download Assignment
                      </a>

                      <a href="#" class="control_btn_success" data-toggle="modal" data-target="#assignment-modal" data-id="{{$assignment->id}}">
                          <i class="fa fa-cloud-upload"></i> &nbsp; Resubmit Assignment
                      </a>
                  @elseif($response && $response->status_id > 1)
                    <p style="font-weight: 600">Assignment Marked</p>
                  @else 
                    <a href="{{url('/').'/assignments/'.$assignment->file}}" class="control_btn">
                      <i class="fa fa-cloud-download"></i> &nbsp;  download Assignment
                    </a>

                     <a href="#" class="control_btn_success" data-toggle="modal" data-target="#assignment-modal" data-id="{{$assignment->id}}">
                           <i class="fa fa-cloud-upload"></i> &nbsp;  Submit Assignment
                      </a>
                  @endif
              @else

              <p style="font-weight: 600">Due Date ({{$assignment->expiry_date}}) Expired</p>
              @endif
            </div>


        </div>
        <div class="content-divider"></div>

        <div class="content-body">
          
          <?php $response = $assignment->studentResponse(Auth::user()->id)->first();?>
          
          <p style="font-weight: 600">INSTRUCTIONS</p>
          <P style="margin-bottom: 40px" >{{$assignment->description}}</P>


          <p style="font-weight: 600">MARKING DETAILS</p>
         
          @if($response)
            
            <p style="font-weight: 600">Marker</p>
            <P style="margin-bottom: 10px" >{{$response->marker()->value('name') ?: "NA"}}</P>

            <p style="font-weight: 600">Score</p>
            <P style="margin-bottom: 10px" >{{$response->student_score}}</P>
            
            <p style="font-weight: 600">Remarks</p>
            <P style="margin-bottom: 40px" >{{$response->marker_remarks}}</P>

            
             @if($response->status_id == 3)

              <p style="font-weight: 600">STUDENT RATINGS</p>

              <p style="font-weight: 600">Rating</p>
              <P style="margin-bottom: 10px" >{{$response->marker_rating}}</P>

              <p style="font-weight: 600">Student Remarks</p>
              <P style="margin-bottom: 10px" >{{$response->student_remarks}}</P>

             @elseif($response->status_id == 2)
             <p style="font-weight: 600; margin-bottom: 20px">STUDENT RATINGS</p>

             <a href="#" class="control_btn" data-toggle="modal" data-target="#ratings-modal" data-id="{{$response->id}}" style="margin-left: 0px">
                  <i class="fa fa-star"></i> &nbsp; Rate Marker
              </a>
             
             @endif

          @else
             <p style="font-weight: 600">Not Submitted</p>
          @endif


        </div>
    </div>

           @endsection


<div class="modal fade" id="ratings-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
           <div class="modal-dialog">
               <div class="modal-content">

                   <!--Modal header-->
                   <div class="modal-header">
                       <button data-dismiss="modal" class="close" type="button">
                       <span aria-hidden="true">&times;</span>
                       </button>
                       <h4 class="modal-title page_title_inner">Rate Marker</h4>
                   </div>

                   <!--Modal body-->
                   <div class="modal-body">
                     <div class="row">
                       <form method="POST" action="{{url('students/assignments/rate-marker')}}">
                       {{ csrf_field() }}
                       <div class="col-sm-12">
                           <input type="hidden" name="response_id" id="response_id" value="">
                           <div class="form-group auth_login_forms" style="">
                                <label for="code" class="control-label client_label auth_label">Ratings</label>
                                <div class="text_container" style="overflow: hidden;">
                                    <select name="rating" class="auth_input" id="rating">
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                      <option value="5">5</option>
                                    </select>
                                </div>
                           </div>
                           
                           <div class="form-group auth_login_forms" style="">
                                <label for="remarks" class="control-label client_label auth_label">Remarks</label>
                                <div class="text_container">
                                    <textarea name="remarks" class="form-control ex-form auth_input" rows="4" required></textarea>  
                                </div>
                           </div>
                        
                       </div>
                     </div>
                   </div>

                   <!--Modal footer-->
                   <div class="modal-footer">
                       <button class="save_btn pull-right" type="submit">Save</button>
                   </div>
                   </form>
               </div>
           </div>
       </div>


       <div class="modal fade" id="assignment-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
           <div class="modal-dialog">
               <div class="modal-content">

                   <!--Modal header-->
                   <div class="modal-header">
                       <button data-dismiss="modal" class="close" type="button">
                       <span aria-hidden="true">&times;</span>
                       </button>
                       <h4 class="modal-title page_title_inner">Submit Assignment</h4>
                   </div>

                   <!--Modal body-->
                   <div class="modal-body">
                     <div class="row">
                       <form method="POST" action="{{url('students/assignments/submit')}}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="col-sm-12">
                           
                         <input type="hidden" name="assignment_id" id="assignment_id">
                           
                           <div class="form-group auth_login_forms" style="">
                                <label for="email" class="control-label client_label auth_label">Completed Assignment File</label>
                                <div class="text_container">
                                    <input type="file" name="file" class="form-control ex-form auth_input" required>  
                                </div>
                           </div>
                        
                       </div>
                     </div>
                   </div>

                   <!--Modal footer-->
                   <div class="modal-footer">
                       <button class="save_btn" type="submit">Save</button>
                   </div>
                   </form>
               </div>
           </div>
       </div>


@section('scripts')
 <script type="text/javascript">
 $('#assignments').addClass('active-link')

 $('#ratings-modal').on('show.bs.modal', function (e) {
        var assignmentBnt = $(e.relatedTarget); 
        var id =  assignmentBnt.attr('data-id');
        console.log(id);
        $('#response_id').val(id);
     });

 $('#assignment-modal').on('show.bs.modal', function (e) {
        var assignmentBnt = $(e.relatedTarget); 
        var id =  assignmentBnt.attr('data-id');
        console.log(id);
        $('#assignment_id').val(id);
     });

 $('#rating').barrating({
  theme:'bars-square',
  allowEmpty:true,
  emptyValue:0,
  showValues:true
 });

 </script>
 @endsection
