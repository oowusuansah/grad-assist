@extends('layouts.app')

@section('content')


    <div class="content">
        <div class="content-header">
            <span class="page-title">ASSIGNMENTS</span>
            <div class="page-controls">

              @if(Session::has('success'))
                  <div class="alert alert-success">
                      <p>{{Session::get('success','')}}</p>
                  </div>
              @elseif(Session::has('error'))
                  <div class="alert alert-danger">
                      <p>{{Session::get('error','')}}</p>
                  </div>
              @endif

            </div>
        </div>
        <div class="content-divider"></div>
        <div class="content-body">
          
          @foreach($assignments as $assignment)
                         <div class="assignment-card">
                             <a style="font-weight: 600;color: #000;text-decoration: none;" href="{{url('students/assignments/'.$assignment->id.'/details')}}">
                                <h4 style="margin-bottom: 10px"><strong>{{$assignment->title}}</strong></h4>
                             </a>
                             <p style="font-weight: 600; font-size: 12px">{{$assignment->course()->value('name')}}</p>
                             <p style="font-weight: 600">INSTRUCTIONS</p>
                             <P style="margin-bottom: 40px" >{{strlen($assignment->description) > 300 ? substr($assignment->description,0,300) . ' ... ' : $assignment->description}}</P>

                             <p style="font-weight: 600">Active Date: {{$assignment->active_date}} &nbsp;&nbsp;&nbsp; Expiry Date: {{$assignment->expiry_date}}</p>

                             <p>

                                @if($assignment->active == 1 && $assignment->expiry_date >= Carbon\Carbon::today())
                                     <a href="{{url('/').'/assignments/'.$assignment->file}}" class="control_btn pull-right c-btn-sm" style="margin-top: -20px">
                                       <i class="fa fa-cloud-download fa-2x"></i>
                                     </a>

                                    @if($assignment->studentResponse(Auth::user()->id)->first())
                                        <a href="#" class="control_btn_success pull-right c-btn-sm" data-toggle="modal" data-target="#assignment-modal" data-id="{{$assignment->id}}" style="margin-top: -20px">
                                            <i class="fa fa-cloud-upload fa-2x"></i>
                                        </a>
                                    @else
                                        <a href="#" class="control_btn_success pull-right c-btn-sm" data-toggle="modal" data-target="#assignment-modal" data-id="{{$assignment->id}}" style="margin-top: -20px">
                                           <i class="fa fa-cloud-upload fa-2x"></i>
                                        </a>
                                    @endif
                                @else
                                <p style="font-weight: 600; float: right;">Due Date ({{$assignment->expiry_date}}) Expired</p>
                                @endif

                             </p>
                         </div>
                    @endforeach

        </div>
    </div>

@endsection


<div class="modal fade" id="assignment-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
           <div class="modal-dialog">
               <div class="modal-content">

                   <!--Modal header-->
                   <div class="modal-header">
                       <button data-dismiss="modal" class="close" type="button">
                       <span aria-hidden="true">&times;</span>
                       </button>
                       <h4 class="modal-title page_title_inner">Submit Assignment</h4>
                   </div>

                   <!--Modal body-->
                   <div class="modal-body">
                     <div class="row">
                       <form method="POST" action="{{url('students/assignments/submit')}}" enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="col-sm-12">
                           
                         <input type="hidden" name="assignment_id" id="assignment_id">
                           
                           <div class="form-group auth_login_forms" style="">
                                <label for="email" class="control-label client_label auth_label">Completed Assignment File</label>
                                <div class="text_container">
                                    <input type="file" name="file" class="form-control ex-form auth_input" required>  
                                </div>
                           </div>
                        
                       </div>
                     </div>
                   </div>

                   <!--Modal footer-->
                   <div class="modal-footer" >
                       <button class="save_btn" type="submit" >Save</button>
                   </div>
                   </form>
               </div>
           </div>
       </div>


@section('scripts')

 <script type="text/javascript">

 $('#assignments').addClass('active-link')

    $('#assignment-modal').on('show.bs.modal', function (e) {
        var assignmentBnt = $(e.relatedTarget); 
        var id =  assignmentBnt.attr('data-id');
        console.log(id);
        $('#assignment_id').val(id);
     });

   
</script>

@endsection