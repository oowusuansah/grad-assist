@extends('layouts.app')

@section('content')


<div class="content">
        <div class="content-header">
            <span class="page-title">ASSIGMENTS</span>

             @if(Session::has('success'))
                <div class="alert alert-success">
                      <p>{{Session::get('success','')}}</p>
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">
                      <p>{{Session::get('error','')}}</p>
                </div>
            @endif


            <div class="page-controls">
              <a href="{{url('professors/assignments/create')}}" class="control_btn pull-right"> <i class="fa fa-plus-square"></i> &nbsp; create Assignment</a>
            </div>


        </div>
        <div class="content-divider"></div>

        <div class="content-body">
          
          <table class="table table-striped">
              <thead>
                  <th>#</th>
                  <th>ASSIGMENT</th>
                  <th>COURSE</th>
                  <th>DUE DATE</th>
                  <th>SUBMITTED</th>
                  <th>MARKED</th>
                  <th>HIGHEST SCORE</th>
                  <th>ACTIONS</th>

              </thead>

              <tbody>
                 
                  @foreach($assignments as $assignment)
                        <tr>
                            <td></td>
                            <td><a href="{{url('professors/assignments/'.$assignment->id.'/details')}}">{{$assignment->title}}</a></td>
                            <td>{{$assignment->course()->value('name')}}</td>
                            <td>{{$assignment->expiry_date}}</td>
                            <td>{{$assignment->responses()->count()}}</td>
                            <td>{{$assignment->responses()->where('status_id','>',1)->count()}}</td>
                            <td>{{$assignment->responses()->where('status_id','>',1)->max('student_score')}}</td>
                            <td> 
                              <a href="{{url('professors/assignments/'.$assignment->id.'/edit')}}">
                                  <img style="width: 20px" src="{{asset('img/edit.png')}}">
                              </a>
                            </td>
                        </tr>
                    @endforeach
              </tbody>
          </table>

        </div>
    </div>
@endsection


@section('scripts')
 <script type="text/javascript">
 $('#assignments').addClass('active-link')
 </script>
 @endsection
