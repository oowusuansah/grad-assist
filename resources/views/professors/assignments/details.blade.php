@extends('layouts.app')

@section('content')


<div class="content">
        <div class="content-header">
            <span class="page-title">{{$assignment->title}}</span>

             @if(Session::has('success'))
                <div class="alert alert-success">
                      <p>{{Session::get('success','')}}</p>
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">
                      <p>{{Session::get('error','')}}</p>
                </div>
            @endif


            <div class="page-controls"></div>


        </div>
        <div class="content-divider"></div>

        <div class="content-body">
          
          <p style="font-weight: 600">INSTRUCTIONS</p>
          <P style="margin-bottom: 40px" >{{$assignment->description}}</P>


          <table class="table table-striped">
              <thead>
                  <th>STUDENT NAME</th>
                  <th>SUBMITTED DATE</th>
                  <th>MARKER</th>
                  <th>SCORE</th>
              </thead>

              <tbody>
                 
                  @foreach($assignment->responses()->get() as $response)
                        <tr>
                            <td>{{$response->student()->value('name')}}</td>
                            <td>{{$response->created_at}}</td>
                            <td>{{$response->marker()->value('name') ?: "NA"}}</td>
                            <td>{{$response->student_score}}</td>
                        </tr>
                    @endforeach
              </tbody>
          </table>

        </div>
    </div>
@endsection

<div class="modal fade" id="score-modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true">
           <div class="modal-dialog">
               <div class="modal-content">

                   <!--Modal header-->
                   <div class="modal-header">
                       <button data-dismiss="modal" class="close" type="button">
                       <span aria-hidden="true">&times;</span>
                       </button>
                       <h4 class="modal-title page_title_inner">Submit Score</h4>
                   </div>

                   <!--Modal body-->
                   <div class="modal-body">
                     <div class="row">
                       <form method="POST" action="{{url('assistants/assignments/submit-score')}}">
                       {{ csrf_field() }}
                       <div class="col-sm-12">
                           <input type="hidden" name="response_id" id="response_id" value="">
                           <div class="form-group" style="">
                                <label for="code" class="control-label client_label">Score</label>
                                <input type="number" name="score" class="form-control ex-form" required>  
                           </div>
                           
                           <div class="form-group" style="">
                                <label for="remarks" class="control-label client_label">Remarks</label>
                                <textarea name="remarks" class="form-control ex-form" rows="4" required></textarea>  
                           </div>
                        
                       </div>
                     </div>
                   </div>

                   <!--Modal footer-->
                   <div class="modal-footer" style="padding-right: 65px;">
                       <button class="btn btn-primary  submit-button" type="submit">Save</button>
                       <button data-dismiss="modal" class="btn btn-default  dismiss-button" type="button">Close</button>
                   </div>
                   </form>
               </div>
           </div>
       </div>

@section('scripts')
 <script type="text/javascript">
 $('#assignments').addClass('active-link')

 $('#score-modal').on('show.bs.modal', function (e) {
        var assignmentBnt = $(e.relatedTarget); 
        var id =  assignmentBnt.attr('data-id');
        console.log(id);
        $('#response_id').val(id);
     });

 </script>
 @endsection
