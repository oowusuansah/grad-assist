@extends('layouts.app')

@section('content')

<div class="content">
        <div class="content-header">
            <span class="page-title">Edit Assignment</span>

             @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>Score saved Successsfully</p>
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger">
                    <p>Score failed to save. Try Again</p>
                </div>
            @endif


            <div class="page-controls"></div>


        </div>
        <div class="content-divider"></div>

        <div class="content-body">
          <div class="col-md-8">
          <form method="POST" action="{{url('professors/assignments/update')}}" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  
                  <input type="hidden" name="assignment_id" value="{{$assignment->id}}">
                   <div class="form-group col-md-12 auth_login_forms" style="">
                       <label class="control-label client_label auth_label">Course</label>

                       <div class="text_container">

                           <select name="course_id" required class="form-control auth_input chosen-select-width">
                               <option  selected value="" disabled>Select Course</option>
                                   @foreach($courses as $course)
                                    <option  value="{{$course->id}}" {{ $course->id == $assignment->course_id ? 'selected' : ''}}>
                                        {{$course->code. '-' .$course->name}}
                                    </option>
                                   @endforeach
                            </select> 
                        </div>      
                   </div>
                   <div class="form-group auth_login_forms col-md-12" style="">
                        <label for="phone" class="control-label client_label auth_label">Title</label>
                        <div class="text_container">

                            <input type="text" name="title" value="{{$assignment->title}}" class="form-control auth_input ex-form" required="required">  
                        </div>
                   </div>

                   <div class="form-group auth_login_forms col-md-6" style="">
                        <label for="location" class="control-label client_label auth_label">Active Date</label>
                        <div class="text_container">

                            <input type="date" name="active_date" value="{{$assignment->active_date}}" class="form-control auth_input" required="required"> 
                        </div> 
                   </div>

                   <div class="form-group auth_login_forms col-md-6" style="">
                        <label for="location" class="control-label client_label auth_label">Expiry Date</label>
                        <div class="text_container">

                            <input type="date" name="expiry_date" value="{{$assignment->expiry_date}}" class="form-control auth_input" required="required">
                        </div>  
                   </div>

                    <div class="form-group auth_login_forms col-md-12" style="">
                        <label for="location" class="control-label client_label auth_label">File</label>
                        <div class="text_container">

                            <input type="file" accept="application/pdf" name="file" class="form-control auth_input">  
                        </div>
                   </div>

                   <div class="form-group auth_login_forms col-md-12" style="">
                       <label class="control-label">Assistants</label>

                       <div class="text_container">

                           <select name="assistants[]" multiple required class="form-control auth_input chosen-select-width">
                               @foreach($assistants as $assistant)
                                <option  value="{{$assistant->id}}" {{in_array($assistant->id,$assignment->markers()->pluck('user_id')->toArray()) ? 'selected' : ''}}>
                                    {{$assistant->name}}
                                </option>
                               @endforeach
                           </select>
                        </div>
                   </div>

                    <div class="form-group auth_login_forms col-md-12" style="">
                        <label for="location" class="control-label client_label auth_label">Assignment Intructions</label>
                        <div class="text_container">

                            <textarea class="form-control auth_input" rows="5" name="description">{{$assignment->description}}</textarea>
                        </div>
                   </div>  


                   <div class="form-group col-md-12" style="">
                        <button type="submit" class="save_btn"> Save </button>
                   </div>
          </form>
      </div>
        </div>
    </div>

@endsection

@section('scripts')
 <script type="text/javascript">
 $('#assignments').addClass('active-link')
 </script>
 @endsection


 
{{-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum. --}}