@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Course</div>

                <div class="panel-body">
                    <div class="form-group col-md-6" style="">
                        <label class="control-label client_label">Code</label>
                        <input type="date" name="code" class="form-control ex-form" required="required">  
                    </div>

                    
                    <div class="form-group col-md-6" style="">
                         <label for="phone" class="control-label client_label">Name</label>
                         <input type="text" name="name" class="form-control ex-form" required="required">  
                    </div>

                    <div class="form-group col-md-6" style="">
                         <label for="location" class="control-label client_label">Location</label>
                         <input type="text" name="location" class="form-control ex-form" required="required">  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
