@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="content-header">
            <span class="page-title">OVERVIEW</span>
        </div>
        <div class="content-divider"></div>
        <div class="content-body"></div>
    </div>
@endsection

@section('scripts')
 <script type="text/javascript">
 $('#home').addClass('active-link')
 </script>
 @endsection
