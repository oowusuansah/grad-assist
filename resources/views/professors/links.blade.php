<li class="side-bar-nav-item" id="home">
    <a href="{{url('professors/home')}}"><img src="{{asset('img/overview.PNG')}}"> <span>Overview</span></a>
</li> 

<li class="side-bar-nav-item" id="assignments">
    <a href="{{url('professors/assignments')}}"><img src="{{asset('img/assignment.PNG')}}"> <span>Assignments</span></a>
</li>

<li class="side-bar-nav-item" id="courses">
    <a href="{{url('professors/courses')}}"><img src="{{asset('img/courses.PNG')}}"> <span>Courses</span></a>
</li>