@extends('layouts.auth-1')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            {{-- For profs --}}

            <div class="panel panel-default">
                <div class="panel-heading">Additional Information Professors</div>

                <div class="panel-body">
                    Welcome To Grad Assistant, Please set up your account
                </div>
            </div>
            
            {{-- For assistants --}}

            <div class="panel panel-default">
                <div class="panel-heading">Additional Information For Assistants</div>

                <div class="panel-body">
                    Welcome To Grad Assistant, Please set up your account
                </div>
            </div>

            {{-- For Students--}}


            <div class="panel panel-default">
                <div class="panel-heading">Additional Information for Students</div>

                <div class="panel-body">
                    Welcome To Grad Assistant, Please set up your account and register courses

                    <form method="POST" action="{{url('users/setup/store')}}">
                    {{ csrf_field() }}
                    <div class="col-sm-12">
                        <input type="hidden" name="user_type_id" value="2">
                        <div class="form-group" style="">
                             <label for="email" class="control-label client_label">Name Alias</label>
                             <input type="text" name="alias" class="form-control ex-form" required>  
                        </div>
                        
                        <div class="form-group" style="">
                             <label for="email" class="control-label client_label">Reistered Courses</label>
                             <select name="courses[]" multiple required class="form-control">
                                @foreach($courses as $course)
                                 <option  value="{{$course->id}}">
                                     {{$course->code. '-' .$course->name}}
                                 </option>
                                @endforeach
                             </select>       
                        </div>

                        <div class="form-group" style="">
                             <button class="btn btn-primary  submit-button" type="submit">Save</button>
                        </div>
                     
                    </div>
                </form>
             </div>

            </div>

        </div>
    </div>
</div>
@endsection
