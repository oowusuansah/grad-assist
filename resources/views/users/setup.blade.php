@extends('layouts.auth-1')

@section('content')
        <div class="feature-panel">
           <img  class="logo" src="{{asset('img/logo.PNG')}}">
        </div>
        <div class="main-panel">
            <div class="main-panel-table">
                <div class="main-panel-tabel-cell">
                    <div class="main_login_content">

                        @if(Auth::user()->user_type_id == 2)

                            <div class="login_text">
                                Additional Information for Students
                            </div>
                            <div class="login_text_small">
                                Welcome To Grad Assistant, Please set up your account and select courses
                            </div>

                            {{-- Student --}}

                            <div class="login_form">
                             <form class="form-horizontal" role="form" method="POST" action="{{ url('users/setup/store') }} " enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_type_id" value="2">
                                <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                                    <div class="image-preview" style="">
                                        
                                    </div>
                                    <div class="inp_file">
                                        <label class="pro_img" for="in_file">select image</label>
                                        <input type="file" id="in_file" name="image"> 
                                    </div>
                                </div>



                                <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                                    <label for="email" class="auth_label">Name Alias / Username</label>

                                    <div class="text_container">
                                        <input id="email" type="text" class="form-control auth_input" placeholder="Your alias" name="alias" required>
                                    </div>
                                </div>

                             <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                                <label for="password" class="auth_label">Courses</label>

                                <div class="text_container">
                                    <select name="courses[]" multiple required class="form-control auth_input chosen-select">
                                       @foreach($courses as $course)
                                        <option  value="{{$course->id}}">
                                            {{$course->code. '-' .$course->name}}
                                        </option>
                                       @endforeach
                                    </select>      
                                </div>
                             </div>

                         <div class="form-group" style="margin-left:0px; margin-right:0px">
                            <div class="pull-left">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="identifier" value="2"> Use name Alias
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" style="margin-left:0px; margin-right:0px">
                            <input type="submit" value="Save" class="login_btn">
                        </div>


                             </form>
                        </div>
                        @elseif(Auth::user()->user_type_id == 3)

                        <div class="login_text">
                                Additional Information for Graduate Assistant
                            </div>
                            <div class="login_text_small">
                                Welcome To Grad Assistant, Please set up your account
                            </div>

                            {{-- Student --}}

                            <div class="login_form">
                             <form class="form-horizontal" role="form" method="POST" action="{{ url('users/setup/store') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_type_id" value="3">

                                <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                                    <div class="image-preview" style="">
                                        
                                    </div>
                                    <div class="inp_file">
                                        <label class="pro_img" for="in_file">select image</label>
                                        <input type="file" id="in_file" name="image"> 
                                    </div>
                                </div>


                                <div class="form-group auth_login_forms" style="margin-left:0px; margin-right:0px">
                                    <label for="email" class="auth_label">Name Alias / Username</label>

                                    <div class="text_container">
                                        <input id="email" type="text" class="form-control auth_input" placeholder="Your alias" name="alias" required>
                                    </div>
                                </div>

                                 <div class="form-group" style="margin-left:0px; margin-right:0px">
                                    <div class="pull-left">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="identifier" value="2"> Use name Alias
                                            </label>
                                        </div>
                                    </div>
                                </div>

                        <div class="form-group" style="margin-left:0px; margin-right:0px">
                            <input type="submit" value="Save" class="login_btn">
                        </div>


                             </form>
                        </div>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
@endsection


@section('script')

<script type="text/javascript">
    
            $(document).on('change','#in_file', function(event) {
       
           console.log(this.files.length);
           for (var i = this.files.length - 1; i >= 0; i--) {

               $('.image-preview').html('<img style="height: 100px;width:100px;border-radius: 50px" src="' + window.URL.createObjectURL(this.files[i]) + '">');
               // console.log(window.URL.createObjectURL(this.files[i]));
           }

        }); 

</script>
@endsection