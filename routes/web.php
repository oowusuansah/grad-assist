<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('logout', 'Auth\LoginController@logout');
Auth::routes();



Route::post('users/setup/store', 'UsersController@saveDetails');


Route::group(['middleware'=>['auth','setup']], function(){

	Route::get('users/setup', 'UsersController@setup');

});



Route::group(['middleware'=>['auth','setup','filter']], function(){



	
	Route::get('/home', 'HomeController@index')->name('home');

	//All routes for professor

	Route::post('users/profile/update', 'UsersController@updateDetails');

	Route::get('professors/home', 'ProfessorsController@index');
	Route::get('professors/courses', 'ProfessorsController@courses');
	Route::post('professors/courses/store', 'ProfessorsController@storeCourse');


	Route::get('professors/assignments', 'ProfessorsController@assignments');
	Route::get('professors/assignments/create', 'ProfessorsController@createAssignment');
	Route::get('professors/assignments/{assignment_id}/edit', 'ProfessorsController@editAssignment');
	Route::post('professors/assignments/store', 'ProfessorsController@storeAssignment');
	Route::post('professors/assignments/update', 'ProfessorsController@updateAssignment');
	Route::get('professors/assignments/{assignment_id}/details', 'ProfessorsController@submittedAssignment');



	//All routes for students

	Route::get('students/home', 'StudentsController@index');
	Route::get('students/assignments', 'StudentsController@assignments');
	Route::get('students/courses', 'StudentsController@courses');
	Route::post('students/courses/register', 'StudentsController@registerCourses');
	Route::post('students/assignments/submit', 'StudentsController@submitAssignments');
	Route::post('students/assignments/rate-marker', 'StudentsController@rateMarker');
	Route::get('students/assignments/{assignment_id}/details', 'StudentsController@assignmentDetails');



	//All routes for assistants

	Route::get('assistants/home', 'AssistantsController@index');
	Route::get('assistants/assignments', 'AssistantsController@assignments');
	Route::get('assistants/leaderboard', 'AssistantsController@leaderboard');
	Route::post('assistants/assignments/submit-score', 'AssistantsController@submitAssignmentScore');
	Route::get('assistants/assignments/{assignment_id}/submitted', 'AssistantsController@submittedAssignment');


});