<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Response;
class AddScoreToUsersTableForAssistants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->decimal('total_score')->unsigned()->nullable();
        });

        foreach (User::all() as $user) {
            $totalScore = $user->markedAssignments()->where('status_id',3)->sum('marker_rating');
            $user->update(['total_score'=>$totalScore]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('total_score');
        });
    }
}
