<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('assignment_id')->unsigned()->index()->nullable();
            $table->integer('student_id')->unsigned()->index()->nullable();
            $table->integer('marker_id')->unsigned()->index()->nullable();
            $table->integer('semester_id')->unsigned()->index()->nullable();
            $table->integer('status_id')->unsigned()->index()->nullable();
            $table->string('file', 100)->nullable();
            $table->integer('student_score')->unsigned()->default(0);
            $table->integer('marker_rating')->unsigned()->default(0);
            $table->text('marker_remarks')->nullable();
            $table->text('student_remarks')->nullable();
         

            $table->foreign('assignment_id')->references('id')->on('assignments')->onDelete('set null');
            $table->foreign('student_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('marker_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete('set null');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
