<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSemeterIdToCourseRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_register', function (Blueprint $table) {
            $table->integer('semester_id')->unsigned()->index()->nullable();
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
 
        Schema::table('course_register', function (Blueprint $table) {
            $table->dropForeign('course_register_semester_id_foreign');
            $table->dropIndex('course_register_semester_id_index');
            $table->dropColumn('semester_id');
        });

        Schema::enableForeignKeyConstraints();
    }
}
