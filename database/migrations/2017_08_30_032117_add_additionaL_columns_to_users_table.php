<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionaLColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('nsid', 20)->nullable();
            $table->string('alias', 50)->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('identifier')->unsigned()->default(1);
            $table->tinyInteger('active')->unsigned()->default(0);
            $table->integer('user_type_id')->unsigned()->index()->nullable();
            $table->foreign('user_type_id')->references('id')->on('user_types')->ondelete('set null');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_user_type_id_foreign');

            $table->dropColumn('user_type_id');
            $table->dropColumn('nsid');
            $table->dropColumn('active');
            $table->dropColumn('alias');
            $table->dropColumn('image');
            $table->dropColumn('deleted_at');
        });
    }
}
