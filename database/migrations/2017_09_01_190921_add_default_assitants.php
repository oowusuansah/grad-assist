<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultAssitants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
        ['name'=>'Oscar Nana Ansah','email'=>'fboscar91@gmail.com','password'=>bcrypt('testpass'),'user_type_id'=>3,'nsid'=>'ona123','active'=>1],
        ['name'=>'Oscar Kwabena Ansah','email'=>'fboscar92@gmail.com','password'=>bcrypt('testpass'),'user_type_id'=>3,'nsid'=>'oka123','active'=>1]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
