<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultSemester extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('semesters')->insert([
            'name'=>'2017/18 First Semester',
            'start_date'=>'2017-09-20',
            'end_date'=>'2017-12-20',
            'active'=>1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('semesters')->truncate();
    }
}
