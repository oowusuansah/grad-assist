<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('file', 100)->nullable();
            $table->tinyInteger('active')->unsigned()->default(0);
            $table->date('active_date')->nullable();
            $table->date('expiry_date')->nullable();
            $table->integer('course_id')->unsigned()->index()->nullable();
            $table->integer('professor_id')->unsigned()->index()->nullable();
            $table->integer('semester_id')->unsigned()->index()->nullable();

            $table->foreign('course_id')->references('id')->on('courses')->onDelete('set null');
            $table->foreign('professor_id')->references('id')->on('courses')->onDelete('set null');
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete('set null');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
