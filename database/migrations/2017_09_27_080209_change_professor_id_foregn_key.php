<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProfessorIdForegnKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('assignments', function (Blueprint $table) {
            $table->dropForeign('assignments_professor_id_foreign');
            $table->foreign('professor_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('assignments', function (Blueprint $table) {
            $table->dropForeign('assignments_professor_id_foreign');
            $table->foreign('professor_id')->references('id')->on('courses')->onDelete('set null');
        });
    }
}
