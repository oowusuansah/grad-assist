<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Request;
use Log;

class RoutesFilterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // Log::debug((array)Auth::guard());

        $user = Auth::user();

        if($user->active == 0){
            return redirect('users/setup');
        }

        if($user->user_type_id != null && $user->user_type_id == 3 && Request::segment(1) != 'assistants' && $request->path() != 'users/profile/update'){
            return redirect('assistants/home');
        }
        

        if($user->user_type_id != null && $user->user_type_id == 2 && Request::segment(1) != 'students' && $request->path() != 'users/profile/update'){
            return redirect('students/home');
        }

        if($user->user_type_id != null && $user->user_type_id == 1 && Request::segment(1) != 'professors' && $request->path() != 'users/profile/update'){
            return redirect('professors/assignments');
        }

        return $next($request);
    }
}
