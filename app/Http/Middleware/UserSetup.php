<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Log;

class UserSetup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
        Log::debug($request->path());
        
        if($user->active == 0 && $request->path() != 'users/setup'){
            return redirect('users/setup');
        }

        return $next($request);
    }
}
