<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Assignment;
use App\Response;
use App\Group;
use App\Course;
use App\Semester;
use App\UserType;
use Auth;
use App\User;
use Session;

class ProfessorsController extends Controller
{
    

    public function index()
    {
    	return view('professors.index')->withCourses(Auth::user()->professorCourses()->get());	
    }

    public function assignments()
    {
    	$prof = Auth::user();
    	$assignments = $prof->professorAssignments()->get();
    	return view('professors.assignments.index')->withAssignments($assignments);
    }


    public function createAssignment()
    {   
          $courses = Auth::user()->professorCourses()->get();
          $assistants = User::where('user_type_id',3)->get();

          return view('professors.assignments.create')
                    ->withCourses($courses)
                    ->withAssistants($assistants);
    
    }


    public function editAssignment($assignmentId)
    {	
    	  $courses = Auth::user()->professorCourses()->get();
    	  $assistants = User::where('user_type_id',3)->get();

    	  return view('professors.assignments.edit')
                    ->withAssignment(Assignment::find($assignmentId))
    	  			->withCourses($courses)
    	  			->withAssistants($assistants);
	
    }


    public function storeAssignment(Request $request)
    {

        // return $request->all();

        $ext = $request->file('file')->getClientOriginalExtension();
        $file = 'assignment-file'.time().'.'.$ext;
        $request->file('file')->move('assignments/',$file);

        $assignment = Assignment::create([
            'professor_id' => Auth::user()->id,
            'semester_id' => Semester::currentSemester()->id,
            'active' => 1,
            'course_id' => $request->get('course_id'),
            'title'=> $request->get('title'),
            'description'=>$request->get('description'),
            'active_date'=>$request->get('active_date'),
            'expiry_date'=>$request->get('expiry_date'),
            'file'=> $file

        ]);

        $assignment->markers()->attach($request->get('assistants'));

        //send email to markers
        //notify students of a new assignmnent
        Session::flash('success', 'Saved');
        
        return redirect('professors/assignments');
    }

    public function updateAssignment(Request $request)
    {

        $assignment = Assignment::find($request->get('assignment_id'));

    	$assignment->update([
    		'professor_id' => Auth::user()->id,
    		'semester_id' => Semester::currentSemester()->id,
    		'active' => 1,
    		'course_id' => $request->get('course_id'),
    		'title'=> $request->get('title'),
    		'description'=>$request->get('description'),
    		'active_date'=>$request->get('active_date'),
    		'expiry_date'=>$request->get('expiry_date')
    	]);

        if($request->hasFile('file')){

            $ext = $request->file('file')->getClientOriginalExtension();
            $file = 'assignment-file'.time().'.'.$ext;
            $request->file('file')->move('assignments/',$file);
            $assignment->file = $file;
            $assignment->save();
        }
        

    	$assignment->markers()->sync($request->get('assistants'));

        //send email to markers
        //notify students of a new assignmnent
        Session::flash('success', 'Saved');
        
    	return redirect('professors/assignments');
    }

    public function courses()
    {
    	return view('professors.courses.index')->withCourses(Auth::user()->professorCourses()->get());
    }

    public function submittedAssignment($assignmentId)
    {
        return view('assistants.assignments.details')->withAssignment(Assignment::find($assignmentId));
    }


    public function storeCourse(Request $request)
    {
    	$course = Course::whereCode($request->get('code'))->first();

    	if($course){
    		Session::flash('error', 'Course Already Exists');
    		return back();
    	}else{

    		Session::flash('success', 'Course Created Successfully');

    		$course = Course::create([
    			'name' => $request->get('name'),
    			'code' => $request->get('code')
    		]);

    		$prof = Auth::user();
    		$prof->professorCourses()->attach($course->id);

    		return back();
    	}
    	
    }

}
