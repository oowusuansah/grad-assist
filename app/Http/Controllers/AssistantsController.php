<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Assignment;
use App\Response;
use App\Group;
use App\Course;
use App\Semester;
use App\UserType;
use Auth;
use App\User;
use Session;

class AssistantsController extends Controller
{
    

    public function index()
    {
    	       $assistant = Auth::user();
                $totalRating = $assistant->markedAssignments()->where('status_id',3)->count();
                $avgScore = round($assistant->markedAssignments()->where('status_id',3)->avg('marker_rating'),2);
                $maxScore = $assistant->markedAssignments()->where('status_id',3)->max('marker_rating');
                $minScore = $assistant->markedAssignments()->where('status_id',3)->min('marker_rating');


                $courses = Course::whereIn('id',$assistant->assistantAssignments()->select('course_id')->distinct()->pluck('course_id'))->get();

                $avgCourseRating = [];
                $courseCodes = [];

                $index = 0;
                foreach ($courses as $course) {
                    $courseCodes[$index] = $course->code;
                    $avg = $assistant->markedAssignments()->whereIn('assignment_id',$course->assignments()->where('active','>',0)->pluck('id'))->avg('marker_rating');
                    $avgCourseRating[$index] = round($avg,2);
                    $index ++;
                }


                return view('assistants.index')->with('totalRating',$totalRating)
                                              ->with('avgScore',$avgScore)
                                              ->with('maxScore',$maxScore)
                                              ->with('minScore',$minScore)
                                              ->with('codes',$courseCodes)   
                                              ->with('avgs',$avgCourseRating); 	
    }

    public function assignments()
    {
    	$assistant = Auth::user();
    	$assignments = $assistant->assistantAssignments()->get();
    	return view('assistants.assignments.index')->withAssignments($assignments);
    }

    public function submittedAssignment($assignmentId)
    {
    	return view('assistants.assignments.details')->withAssignment(Assignment::find($assignmentId));
    }

    public function submitAssignmentScore(Request $request)
    {
    	$response = Response::find($request->get('response_id'));
    	
    	if($response){

    		$response->update([
    				'student_score'=>$request->get('score'	),
    				'marker_remarks'=>$request->get('remarks'),
    				'marker_id'=>Auth::user()->id,
    				'status_id'=>2

    		]);

    		Session::flash('success', 'Saved');

    	}else{

    		Session::flash('error', 'failed');
    		    
    	}

    	return back();

    }


    public function leaderboard()
    {
        
        $topUsers = User::where('user_type_id',3)->orderBy('total_score','desc')->get();
        $ranking = [];
        $rank = 1;
        foreach ($topUsers as $user) {

            $top['id'] = $user->id;
            $top['name'] = $user->name;
            $top['image'] = $user->image;
            $top['total_score'] = $user->total_score;
            $top['assignments'] = $user->markedAssignments()->count();
            $top['rank'] = $rank++;

            array_push($ranking, $top);

        }


        return view('assistants.leaderboard')->with('rankings',$ranking);
    }

}
