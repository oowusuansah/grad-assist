<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Assignment;
use App\Response;
use App\Group;
use App\Course;
use App\Semester;
use App\UserType;
use Auth;
use App\User;
use Session;
use App\CourseRegister;

class StudentsController extends Controller
{
    
 
    public function index()
    {
        $student = Auth::user();
        $registeredCourses = $student->registeredCourses()->whereSemesterId(Semester::currentSemester()->id)->pluck('course_id');
        $assignmentsCount = Assignment::whereIn('course_id',$registeredCourses)->where('active','>',0)->count();
        $avgScore = round($student->assignmentResponses()->avg('student_score'),2);
        $maxScore = $student->assignmentResponses()->max('student_score');
        $minScore = $student->assignmentResponses()->where('status_id','>',1)->min('student_score');

        $courses = Course::whereIn('id',$student->registeredCourses()->whereSemesterId(Semester::currentSemester()->id)->pluck('course_id'))->get();
        $avgCourseScore = [];
        $courseCodes = [];

        $index = 0;
        foreach ($courses as $course) {
            $courseCodes[$index] = $course->code;
            $avg = $student->assignmentResponses()->whereIn('assignment_id',$course->assignments()->where('active','>',0)->pluck('id'))->avg('student_score');
            $avgCourseScore[$index] = round($avg,2);
            $index ++;
        }


        // $data =  ['codes'=>$courseCodes,'avgs'=>$avgCourseScore];

    	return view('students.index')->with('assignmentsCount',$assignmentsCount)
                                      ->with('avgScore',$avgScore)
                                      ->with('maxScore',$maxScore)
                                      ->with('minScore',$minScore)
                                      ->with('codes',$courseCodes)   
                                      ->with('avgs',$avgCourseScore);	
    }

    public function assignments()
    {
    	$student = Auth::user();
    	$registeredCourses = $student->registeredCourses()->whereSemesterId(Semester::currentSemester()->id)->pluck('course_id');
    	$assignments = Assignment::whereIn('course_id',$registeredCourses)->where('active','>',0)->get();
    	return view('students.assignments.index')->withAssignments($assignments);
    }

    public function submitAssignments(Request $request)
    {
    	

    	$ext = $request->file('file')->getClientOriginalExtension();
    	$file = 'assignment-file'.time().'.'.$ext;
    	$request->file('file')->move('assignments/submitted/',$file);

    	$assignment = Assignment::find($request->get('assignment_id'));
    	$student = Auth::user();
    	$assignmentResponse = $assignment->studentResponse($student->id)->first();
    	if($assignmentResponse){

    		$assignmentResponse->update(['file'=>$file]);

    	}else{
    		$assignmentResponse = Response::create([
    			'assignment_id' => $request->get('assignment_id'),
    			'student_id' => $student->id,
    			'semester_id' => $assignment->semester_id,
    			'status_id' => 1,
    			'file' => $file
    		]);

    	}
    	Session::flash('success', 'Assignment submitted Successfully');
    	return  redirect('students/assignments');
    }

     public function courses()
     {

        $registeredCourses =  Course::whereIn('id',Auth::user()->registeredCourses()->whereSemesterId(Semester::currentSemester()->id)->pluck('course_id'))->get();
         return view('students.courses.index')
                    ->with('registeredCourses',$registeredCourses)
                    ->with('courses',Course::whereNotIn('id',Auth::user()->registeredCourses()->pluck('course_id')->toArray())->get());
     }


     public function registerCourses(Request $request)
     {
        $user = Auth::user();
         $courses = $request->get('courses');
         foreach ($courses as $courseId) {
            $user->registeredCourses()->create(['course_id'=>$courseId,'semester_id'=>Semester::currentSemester()->id]);
         }

         return back();
     }


      public function assignmentDetails($assignmentId)
    {
        return view('students.assignments.details')->withAssignment(Assignment::find($assignmentId));
    }

    public function rateMarker(Request $request)
    {
        $response = Response::find($request->get('response_id'));
        
        if($response){

            $response->update([
                    'marker_rating'=>$request->get('rating'),
                    'student_remarks'=>$request->get('remarks'),
                    'status_id'=>3
            ]);

            $marker = $response->marker()->first();
            $marker->total_score += $request->get('rating');
            $marker->save();

            Session::flash('success', 'Saved');

        }else{

            Session::flash('error', 'failed');
                
        }

        return back();
    }
}
