<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Assignment;
use App\Response;
use App\Group;
use App\Course;
use App\Semester;
use App\UserType;
use Auth;
use App\User;
use Session;
use App\CourseRegister;


class UsersController extends Controller
{
    
	public function setup()
	{
		return view('users.setup')->withCourses(Course::all());
	}


	public function saveDetails(Request $request)
	{

		// return $request->all();


		$user = Auth::user();

		$fileName = "";
		
		if($request->hasFile('image')){
            $ext = $request->file('image')->getClientOriginalExtension();
            $fileName = 'user-image-'.time().'.'.$ext;
            $request->file('image')->move('media/users/',$fileName);
        }

		$user->update([
			'alias'=>$request->get('alias'),
			'active'=>1,
			'image'=>$fileName,
			'identifier'=> $request->get('identifier') ?: '1'
		]);

		if($user->user_type_id == 2){
			$courses = $request->get('courses');
			foreach ($courses as $courseId) {
				$user->registeredCourses()->create(['course_id'=>$courseId,'semester_id'=>Semester::currentSemester()->id]);
			}
		}

		return redirect('home');
	}


	public function updateDetails(Request $request)
	{
			
			$user = Auth::user();

			$fileName = $user->image;
			
			if($request->hasFile('image')){
	            $ext = $request->file('image')->getClientOriginalExtension();
	            $fileName = 'user-image-'.time().'.'.$ext;
	            $request->file('image')->move('media/users/',$fileName);
	        }

			$user->update([
				'name'=>$request->get('name'),
				'alias'=>$request->get('alias'),
				'image'=>$fileName,
			]);

    		Session::flash('success', 'Saved');
    		return back();
	}
}
