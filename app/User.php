<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function professorAssignments()
    {
        return $this->hasMany('App\Assignment','professor_id');
    }

    public function assistantAssignments()
    {
        return $this->belongsToMany('App\Assignment','assignment_marker','user_id');
    }

    public function userType()
    {
        return $this->belongsTo('App\UserType');
    }


    public function assignmentResponses()
    {
        return $this->hasMany('App\Response','student_id');
    }


    public function markedAssignments()
    {
        return $this->hasMany('App\Response','marker_id');
    }

    public function registeredCourses()
    {
        return $this->hasMany('App\CourseRegister');
    }

    public function professorCourses()
    {
        return $this->belongsToMany('App\Course','professor_course','user_id');
    }
}
