<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Response extends Model
{
    use SoftDeletes;
    protected $guarded = [];



    public function marker()
    {
    	return $this->belongsTo('App\User','marker_id');
    }

    public function student()
    {
    	return $this->belongsTo('App\User','student_id');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Semester');
    }

    public function assignment()
    {
    	return $this->belongsTo('App\Assigment');
    }
}
