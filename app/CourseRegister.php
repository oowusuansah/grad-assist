<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseRegister extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'course_register';


    public function course()
    {
    	return $this->belongsTo('App\Course');
    }


    public function student()
    {
    	return $this->belongsTo('App\User');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Semester');
    }
}
