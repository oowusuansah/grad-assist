<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;
    protected $guarded = [];


    public function professor()
    {
    	return $this->belongsTo('App\User','professor_id');
    }

    public function markers()
    {
    	return $this->belongsToMany('App\User','assignment_marker','assignment_id');
    }

    public function responses()
    {
    	return $this->hasMany('App\Response');
    }

    public function course()
    {
    	return $this->belongsTo('App\Course');
    }

    public function semester()
    {
    	return $this->belongsTo('App\Semester');
    }

    public function studentResponse($studentId)
    {
        return $this->hasMany('App\Response')->whereStudentId($studentId);
    }

    public function markerResponses($markerId)
    {
        return $this->hasMany('App\Response')->whereMarkerId($markerId);

    }
}
