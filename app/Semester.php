<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Semester extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function assignments()
    {
    	return $this->hasMany('App\Assignment');
    }

    public static function currentSemester()
    {
    	return Semester::whereActive(1)->first();
    }
}
