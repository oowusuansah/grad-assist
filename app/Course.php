<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function semester()
    {
    	return $this->belongsTo('App\Semester');
    }


    public function assignments()
    {
    	return $this->hasMany('App\Assignment');
    }

    public function registeredStudents()
    {
    	return $this->hasMany('App\CourseRegister');
    }

    public function professors()
    {
    	return $this->belongsToMany('App\User','professor_course','course_id');
    }

    
}
